package com.task.activity.dao;

import com.task.activity.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface UserRepository extends MongoRepository<User,String> {
 Optional<User> findByUserID(String UserID);

 void deleteUserByUserID(String userID);

 Optional<User> findByPhone(String phone);

 Optional<User> findByEmail(String email);
}
