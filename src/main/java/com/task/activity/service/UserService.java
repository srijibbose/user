package com.task.activity.service;

import com.task.activity.model.User;
import org.springframework.http.ResponseEntity;

public interface UserService {
     ResponseEntity UpdateUser(String userID, User userUpdate);
    
     ResponseEntity AddUser(User user) ;

     ResponseEntity FetchAllUsers();

     ResponseEntity DeleteUser(String userID);

     ResponseEntity FetchUserByUserID(String userID);

     ResponseEntity deleteAll();

     ResponseEntity FetchUserByPhone(String phone);

}
