package com.task.activity.utilities;

import org.springframework.stereotype.Component;

@Component
public class PhoneValidator {
    public boolean isValidPhone(String phone){
        String mobileNoRegex = "[1]{3}\\d{7}|[2]\\d{9}|[6-9]\\d{9}";

         return phone != null && phone.matches(mobileNoRegex);
    }
}
