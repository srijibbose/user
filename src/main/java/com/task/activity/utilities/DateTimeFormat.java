package com.task.activity.utilities;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

@Component
public class DateTimeFormat {

    @Value("${internal.time-zone}")
    private String timezone;
    public LocalDateTime getCurrentLocalDateTime(){
        return Instant.now().atZone(ZoneId.of(timezone)).toLocalDateTime();
    }
}
