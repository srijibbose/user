package com.task.activity.controller;

import com.task.activity.model.User;
import com.task.activity.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class UserController {
    @Autowired
    private UserService userService;

    @PostMapping("/activity/add-user")
    public ResponseEntity AddUser(@RequestBody User user)
    {
       return userService.AddUser(user);
    }

    @GetMapping("/activity/get-all-users")
    public ResponseEntity FetchAllUsers()
    {
       return userService.FetchAllUsers();
    }

    @GetMapping("/activity/fetch-user-by-userid/{userID}")
    public ResponseEntity FetchUserByUserID(@PathVariable String userID)
    {
        return userService.FetchUserByUserID(userID);
    }

    @PutMapping("/activity/update-user")
    public ResponseEntity UpdateUser(@RequestHeader(value="userID") String UserID,
                                     @RequestBody User user)
    {
        return userService.UpdateUser(UserID, user);
    }

    @DeleteMapping("/activity/delete/{userID}")
    public ResponseEntity DeleteUser(@PathVariable(value = "userID") String userID)
    {
        return userService.DeleteUser(userID);
    }

    @DeleteMapping("/activity/delete")
    public ResponseEntity deleteAll()
    {
        return userService.deleteAll();
    }


    @GetMapping("/activity/fetch-user-by-phone/{phone}")
    public ResponseEntity FetchUserByPhone(@PathVariable String phone)
    {
        return userService.FetchUserByPhone(phone);

    }

}
